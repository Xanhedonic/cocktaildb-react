import React from "react";

const About = () => {
  return (
    <section className='section about-section'>
      <h1 className='section-title'>about us</h1>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi,
        dignissimos magni fuga maxime Robertas Tupikas quas qui reprehenderit
        repellendus enim minima cum aspernatur, aperiam sunt eum. Quae rerum
        maiores facilis modi accusamus, vitae neque doloremque laudantium ipsa
        consectetur similique. Officia?
      </p>
    </section>
  );
};

export default About;
